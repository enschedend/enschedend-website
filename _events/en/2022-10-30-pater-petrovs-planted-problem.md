---
title: Pater Petrov's Planted Problem
image: banner_09_pater_petrov.png
form: https://docs.google.com/forms/d/e/1FAIpQLSdRXFsOOJcsXDDUxkSZIJBswqkM4hUKxFZ2yTnxFjaPB_wGmA/viewform
soldout: true
---

Are you ready for creepy zombies, a mysterious garden and a (rumored) cursed monastery? Sign up now for the new adventure of EnscheD&D! It's going to be an exciting story full of supernatural phenomena, plants that try to kill you and enough undead to entertain the clerics.

**Date & time: October 30th 2022, 12.00-17.30 at the Volbert in Enschede!**

Entrance is 7,50 for players.
