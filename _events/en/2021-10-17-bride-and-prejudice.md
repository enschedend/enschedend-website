---
title: Bride & Prejudice
image: banner_06_bride_and_prejudice.jpg
form: https://docs.google.com/forms/d/e/1FAIpQLSdXJhiMU09tSc8KZPChMASIyUc38IgsoLpo0APdQO0YUyUkrg/viewform
soldout: true
---

After a much too long break we're very glad to finally get started again!

We hope to see you all at our next Gameday: "Bride and Prejudice"!
