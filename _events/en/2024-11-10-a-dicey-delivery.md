---
title: A Dicey Delivery
image: banner_15_a_dicey_delivery.jpg
form: https://docs.google.com/forms/d/e/1FAIpQLSfD6IRvcDT1bv9Q-hl9hlJBvcKZWM6VfdPi1m-sQ2qefhS8Pw/viewform
soldout: true
---

**Good news: registrations for EnscheD&D are open! Join us on an adventure in a big city, where you have been given the quest to bring a mysterious package to an anonymous person... 🗡️**

> In the city of Arranthis, your party is tasked with delivering a mysterious package from one location to another, and fast! But the city is not without danger. Will you traverse the bridges and waterways of the vibrant Lush? Or will you navigate the crawling alleys and rot farms of the Neath? Time is running out!

*Date & time: November 10 2024 12.00-17.30h at 't Volbert in Enschede.*

You can book your ticket now! Payment follows later via Tikkie.
Do you want to cancel your reservation? Then email or text us.

Some extras:
- Great news! We have a youth table available for this event (age 12-16). Know someone who would enjoy this? Let them know!
- With the new D&D edition coming out this September, we are in a transition phase. For now, we want to ask players to still create characters with the 2014 rules to keep the game balanced. We are still investigating what to do with future events.

### Waiting list

The event is (already) sold out! We're looking into setting up a next
gameday on a larger scale, to do so we do need to know whether there is
enough interest to justify the costs.

In addition, there are always a number of cancellations, signing up for
the waiting list does work!
