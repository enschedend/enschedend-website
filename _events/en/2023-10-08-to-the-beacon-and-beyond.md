---
title: To the Beacon and Beyond
image: banner_12_to_the_beacon_and_beyond.png
form: https://docs.google.com/forms/d/e/1FAIpQLSdkL-ps3tCVjZ0UdF41dhajt67xxXifz4xM8R97ktJgkLWKyQ/viewform
soldout: true
---

Good news: registrations for EnscheD&D are open! This time we have a
thrilling adventure for you in honor of World Space Week (October 4-10).
Grab your mindflayer disguise, supply your space craft, cause we're
going to space, baby!

The astral sea contains many mysteries, jaw-dropping sights and just as
many dangers. Your group is on a mission aboard the Star Striker, a
small ship where you have just awoken from cryosleep. You and your party
receive a distress beacon on your FARRT (Find And Rescue Radar
Transponder) from a party member from past adventures. Without second
thoughts, you set sail to the massive space station where she is being
held. What keeps her there? Guess you’ll find out when you arrive...

For this space fairing adventure you can play any race or class, but if
you happen to own the Spelljammer supplement, the races and background
mentioned in there are a perfect fit for this adventure.

**Date & time: October 8th 2023 12.00-17.30h at 't Volbert in Enschede.**

Entrance is 7,50 for players.
