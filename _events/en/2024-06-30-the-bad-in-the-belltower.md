---
title: The Bad in the Belltower
image: banner_14_bad_in_the_belltower.png
form: https://docs.google.com/forms/d/e/1FAIpQLScJQBGcD5dtbHQaJQm-jAXoZYwSem7HgEJdNJQtFuf-08YDXg/viewform
soldout: true
---

**Good news: registrations for EnscheD&D are open! Join us for an
adventure in a mysterious monastery, with old traps, and a dangerous dragon... 🛕🐉**

In the town of Goldpeak, surrounded by the beautiful rolling hills, life
was pleasant. Well, at least until a few weeks ago, when the people
started noticing strange things. A looming, winged shadow has been
terrifying the villagers. Some say they saw sharp teeth glistening in
the moonlight, others saw bat-like wings, the size of a sailing ship. A
dragon. What does it want? All their treasure, worship, or maybe
destruction of the area?

The townsfolk hired your party to deal with this monster. It seems to
have made the long forgotten Monastery of the Maiden its new lair.
Located on the edge of the Crumbling Cliffs, and reaching it will not be
an easy task... But, heroes as you are, you accepted this quest, and
went on your journey to save the good people of Goldpeak.

*Date & time: June 30 2024 12.00-17.30h at 't Volbert in Enschede.*

**Entrance is 7,50 for players. Payment follows later via Tikkie.**

Do you want to cancel your reservation? Please email or text us.

### Waiting list

The event is (already) sold out. We're looking into setting up a next
gameday on a larger scale, to do so we do need to know whether there is
enough interest to justify the costs of a larger location.

In addition, there are always a number of cancellations, signing up for
the waiting list does work!
