---
title: The Carnival Conundrum
image: banner_13_carnival_conundrum.png
form: https://docs.google.com/forms/d/e/1FAIpQLSevujTld1fAosyTzNERcHSPbl2VnNum8lMJDo8f18nIQ5qZHQ/viewform
soldout: true
---

Good news: registrations for EnscheD&D are open! This time we travel to
a magical carnival, where you have been given a very important task by
the Fantasy Board of Investigation: something is not right with this
circus, and you must find out what. One of the few leads is a poster
that can be seen all over the area...

```
Bungling Bros’ Funfair and Festival Extraordinaire
For all your Dazzling Dreams
- Beazle and Bob
```

For this adventure you can play any printed species or class, including
rare species like fairies, harengon and Loxodon.

*Date & time: March 10 2024 12.00-17.30h at 't Volbert in Enschede.*

Entrance is 7,50 for players.
