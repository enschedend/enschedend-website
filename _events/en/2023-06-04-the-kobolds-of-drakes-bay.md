---
title: The Kobolds of Drake's Bay
image: banner_11_kobolds_of_drakes_bay.jpg
form: https://docs.google.com/forms/d/e/1FAIpQLSeUWGbwrye_wVYjrxKWPvQPw4qp5NuAqjZ475NAuU-x5UZlcA/viewform
soldout: true
---

Good news: registrations for EnscheD&D are open! And double good news: we have extra tables available on location! But do we have enough adventurers in total to deal with a clan of goblins? That remains to be seen...

The quaint hamlet of Drake's Bay has long been the home of proud fishermen and pearl divers. The bronze dragon Izzrylda protected the bay for centuries until she died a few decades ago. She left her hoard to the clan of kobolds who served her. Now, there is only one problem... A mysterious creature has recently disturbed the peace of Drake's Bay, causing the villagers to flee. The party has been recruited by the village to slay whatever monster awaits them, but the kobolds have other plans…

**Date & time: June 4th 2023, 12.00-17.30 at 't Volbert in Enschede!**

Entrance is 7,50 for players.
