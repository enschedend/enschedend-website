---
title: The Kobolds of Drake's Bay
image: banner_11_kobolds_of_drakes_bay.jpg
form: https://docs.google.com/forms/d/e/1FAIpQLSeUWGbwrye_wVYjrxKWPvQPw4qp5NuAqjZ475NAuU-x5UZlcA/viewform
soldout: true
---

Goed nieuws: de inschrijvingen voor EnscheD&D zijn geopend! En dubbel goed nieuws: we hebben extra plekken beschikbaar op locatie! Maar of we in totaal genoeg avonturiers hebben om te dealen met een clan kobolds? Dat is nog te bezien...

Het schilderachtige gehucht Drake's Bay is al lang het thuis van trotse vissers en parelduikers. De bronzen draak Izzrylda beschermde de baai eeuwenlang tot ze een paar decennia geleden stierf. Ze liet haar schat achter in de handen van de clan van Kobolds die haar dienden. Er is nu alleen één probleem... Een mysterieus wezen heeft onlangs de rust van Drake's Bay verstoord, waardoor de dorpelingen zijn gevlucht. Jullie party is door het dorp gerekruteerd om het monster dat hen te wachten staat te verslaan, maar de kobolds hebben andere plannen...

**Datum & tijd: 4 juni 2023, 12.00-17.30u bij 't Volbert in Enschede!**

Entree is 7,50 voor spelers.
