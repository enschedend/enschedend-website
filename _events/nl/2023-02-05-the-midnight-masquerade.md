---
title: The Midnight Masquerade
image: banner_10_midnight_masquerade.png
form: https://docs.google.com/forms/d/e/1FAIpQLSeX6a41Yzrabtd9CLVPREQG_V4BKXYl_T-5-U4wF9MdZpUp3w/viewform
soldout: true
---

Vier met ons de speciale 10e edititie van EnscheD&D: the Midnight Masquerade! Bereid je voor op het gemaskerde bal van je leven. Extravagante gasten, een prachtig paleis, magische illusies… Maar, hoe ben je hier gekomen? En nog belangrijker… Hoe kom je er weer weg?

Midnight Masquerade is een avontuur waarbij je één doel hebt: ontsnappen aan de magische gastheer die jouw groep aan avonturiers wel een passend vermaak vindt in hun paleis. Niets is wat het lijkt!

**Datum & tijd: 5 februari 2023, 12.00-17.30u bij het Volbert in Enschede!**

Entree is 7,50 voor spelers.
