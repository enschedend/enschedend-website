---
title: The Tale of the Mad Mermaid
image: banner_08_mad_mermaid.png
form: https://docs.google.com/forms/d/e/1FAIpQLSdlcp-qf8uXkzQuXWRk3m8cJypgI-s3mbTc918GTiOf6_ZBgA/viewform
soldout: true
---

Majesteuze schepen, het open water, zilte zeelucht, onbewoonde eilanden, gevaarlijke zeemonsters en natuurlijk piraten! Ben jij klaar voor een avontuur op het schip the Mad Mermaid?

**Datum & tijd: 26 juni 2022 12.00-17.30u bij "Het Volbert" in Enschede!**

Je kan vanaf nu je ticket reserveren! Betaling volgt later via Tikkie.

Wil je je reservering annuleren? Stuur ons dan een mailtje of appje.

Hopelijk tot de volgende Gameday!
