---
title: Sheep in the Big City
image: banner_02_sheep_in_the_big_city.jpg
---

Tijdens deze tweede editie van EnscheD&D duik je in een avontuur in de grote stad. Verwacht schapen, schildwachten en geruchten over een ongeluk. Wees welkom in dit bijzonder bizarre avontuur in de stad Two-Helm!

Het maakt niet uit of je beginner bent in Dungeons en Dragons of al jaren speelt, deze middag is voor iedereen die een gezellige middag wil hebben. Geef je snel op via [{{ site.email }}](mailto:{{ site.email }})! Het aantal plaatsen is beperkt.
