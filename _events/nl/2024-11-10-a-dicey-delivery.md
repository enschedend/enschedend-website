---
title: A Dicey Delivery
image: banner_15_a_dicey_delivery.jpg
form: https://docs.google.com/forms/d/e/1FAIpQLSfD6IRvcDT1bv9Q-hl9hlJBvcKZWM6VfdPi1m-sQ2qefhS8Pw/viewform
soldout: true
---

**Goed nieuws: de inschrijvingen voor EnscheD&D zijn open! Kom mee met ons op een avontuur in een grote stad, waar jullie de quest hebben gekregen om een mysterieus pakketje naar een anoniem persoon in de stad te brengen... 🗡️**

> In de stad Arranthis krijgt jouw party de klus om een mysterieus pakketje van de ene naar de andere plek te brengen, met spoed! Maar de stad is niet zonder gevaren. Reizen jullie over de bruggen en waterwegen van het levendige Lush? Of navigeren jullie de smalle steegjes en rotboerderijen van de Neath? De tijd loopt!

*Datum & tijd: 10 november 2024 12.00-17.30u bij 't Volbert in Enschede.*

Je kan vanaf nu je ticket reserveren! Betaling volgt later via Tikkie.
Wil je je reservering annuleren? Stuur ons dan een mailtje of appje.

Wat extra's:

- Leuk nieuws! We hebben voor dit event een jongerentafel beschikbaar (12-16 jaar). Ken iemand die dit leuk zou vinden? Laat ze het weten!
- Met de nieuwe D&D editie die uitkomt deze september, zitten we in een overgangsfase. Voor nu willen we spelers vragen om nog met de 2014 regels characters te maken om het spel gebalanceerd te houden. We onderzoeken nog wat we met komende edities gaan doen.

### Wachtlijst

Het event is (al) uitverkocht. We zijn aan het kijken of we een volgende
gameday groter kunnen opzetten, daarvoor moeten we wel weten of er
voldoende animo is om de hogere kosten te
verantwoorden.

Daarnaast zijn er altijd een aantal afzeggingen, inschrijven voor de
wachtlijst werkt dus!
