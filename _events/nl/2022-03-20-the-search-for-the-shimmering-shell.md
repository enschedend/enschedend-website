---
title: The Search for the Shimmering Shell
image: banner_07_shimmering_shell.png
form: https://docs.google.com/forms/d/e/1FAIpQLSezJDepVgyxZRtPhRYzdTgX8PDSkgM1p9WsTM8Qsaa2pVEHQg/viewform
soldout: true
---

Bereid je voor op wat net niet Valentijnsdag avonturen.

We hopen jullie allemaal weer te kunnen zien bij onze volgende Gameday: "The Search for the Shimmering Shell"!
